<?php

namespace App\Providers;

use App\Http\Contracts\MarkInterface;
use App\Http\Contracts\TypeInterface;
use App\Http\Contracts\DrivetrainInterface;
use App\Http\Contracts\InsuranceInterface;
use App\Http\Repositories\MarkRepository;
use App\Http\Repositories\TypeRepository;
use App\Http\Repositories\DrivetrainRepository;
use App\Http\Repositories\InsuranceRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(MarkInterface::class, MarkRepository::class);
        $this->app->bind(TypeInterface::class, TypeRepository::class);
        $this->app->bind(DrivetrainInterface::class, DrivetrainRepository::class);
        $this->app->bind(InsuranceInterface::class, InsuranceRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
