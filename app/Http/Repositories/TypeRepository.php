<?php
/**
 * Created by PhpStorm.
 * User: kar
 * Date: 5/20/20
 * Time: 8:04 PM
 */

namespace App\Http\Repositories;

use App\Http\Contracts\TypeInterface;
use App\Models\Type;

/**
 * Class TypeRepository
 * @package App\Http\Repositories
 */
class TypeRepository implements TypeInterface
{
    protected $model;

    /**
     * TypeRepository constructor.
     * @param Type $model
     */
    public function __construct(Type $model)
    {
        $this->model = $model;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get($id)
    {
        return $this->model->where('mark_id', $id)->get();
    }
}
