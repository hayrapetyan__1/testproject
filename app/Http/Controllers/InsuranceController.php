<?php

namespace App\Http\Controllers;

use App\Http\Contracts\InsuranceInterface;
use App\Http\Requests\InsuranceRequest;
use App\Http\Services\FileService;
use Illuminate\Http\Request;

/**
 * Class InsuranceController
 * @package App\Http\Controllers
 */
class InsuranceController extends Controller
{
    protected $insurance, $fileService;

    /**
     * InsuranceController constructor.
     * @param InsuranceInterface $insurance
     */
    public function __construct(InsuranceInterface $insurance, FileService $fileService)
    {
        $this->insurance = $insurance;
        $this->fileService = $fileService;
    }

    /**
     * @param InsuranceRequest $request
     */
    public function create(InsuranceRequest $request)
    {
        try {
            $data = $request->all();
            if ($data) {
                $insurance = $this->insurance->create($data);
                return response()->json([
                    'success' => 1,
                    'type'    => 'success',
                    'data'    => $insurance
                ]);
            } else {
                response()->json([
                    'success' => 0,
                    'type'    => 'error',
                    'message' => 'You should send data'
                ]);
            }
        } catch (\Exception $exception) {
            Log::info($exception->getMessage());

            response()->json([
                'success' => 0,
                'type'    => 'error',
                'message' => 'Something went wrong'
            ]);
        }
    }

    /**
     * @return mixed
     */
    public function get()
    {
        $userId = request()->user_id;
        if ($userId) {
            $data =  $this->insurance->getOne($userId);
            return response()->json([
                'success' => 1,
                'type'    => 'success',
                'data'    => $data
            ]);
        } else {
            return response()->json([
                'success' => 0,
                'type'    => 'error',
                'message' => 'Something went wrong'
            ]);
        }
    }

    /**
     * @param Request $request
     */
    public function fileUpload(Request $request)
    {
        $file = $request->file;
        try{
            if ($file) {
                $this->fileService->upload('insurances/images', $file);
                return response()->json([
                    'success' => 1,
                    'type'    => 'success'
                ]);
            } else {
                response()->json([
                    'success' => 0,
                    'type'    => 'error',
                    'message' => 'Something went wrong'
                ]);
            }
        } catch (\Exception $exception) {
            Log::info($exception->getMessage());

            response()->json([
                'success' => 0,
                'type'    => 'error',
                'message' => 'Something went wrong'
            ]);
        }
    }
}
