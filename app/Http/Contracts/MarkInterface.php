<?php
/**
 * Created by PhpStorm.
 * User: kar
 * Date: 5/20/20
 * Time: 6:57 PM
 */

namespace App\Http\Contracts;

/**
 * Interface MarkInterface
 * @package App\Http\Contracts
 */
interface MarkInterface
{
    /**
     * @return mixed
     */
    public function get();
}
