<?php
/**
 * Created by PhpStorm.
 * User: kar
 * Date: 5/20/20
 * Time: 10:09 PM
 */

namespace App\Http\Contracts;

/**
 * Interface DrivetrainInterface
 * @package App\Http\Contracts
 */
interface DrivetrainInterface
{
    /**
     * @param $model
     * @return mixed
     */
    public function get($type);
}
