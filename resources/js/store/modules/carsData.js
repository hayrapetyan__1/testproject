export default {
    state: {
        marks: {},
        types: {},
        drivetrains: {},
    },
    mutations: {
        marksUpdate(state, data) {
            state.marks = data
        },
        typesUpdate(state, data) {
            state.types = data
        },
        drivetrainsUpdate(state, data) {
            state.drivetrains = data
        },
    },
    actions: {
        getMarks(ctx) {
            axios.get('/api/marks')
                .then(response => {
                    ctx.commit('marksUpdate', response.data.data)
                })
        },
        getTypes(ctx, mark) {
            axios.get('/api/types', {
                params: {
                    mark: mark
                }
            }).then(response => {
                ctx.commit('typesUpdate', response.data.data)
            })
        },
        getDrivetrain(ctx, type) {
            axios.get('/api/drivetrains', {
                params: {
                    type: type
                }
            }).then(response => {
                ctx.commit('drivetrainsUpdate', response.data.data)
            })
        }
    },
    getters: {
        carMarks(state) {
            return state.marks
        },
        carTypes(state) {
            return state.types
        },
        carDrivetrains(state) {
            return state.drivetrains
        }
    }
}
