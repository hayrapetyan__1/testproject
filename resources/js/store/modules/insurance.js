export default {
    state: {
        userInsuranceData: {},
    },
    mutations: {
        dataUpdate(state, data) {
            state.userInsuranceData = data
        },
    },
    actions: {
        getUserInsurance(ctx, userId) {
            return new Promise((resolve, reject) => {
                axios.get('/api/insurance/get', {
                    params: {
                        user_id: userId
                    }
                }).then(response => {
                    ctx.commit('dataUpdate', response.data.data)
                    resolve(response.data)
                }).catch(error => {
                    reject(error);
                });
            })
        },
        saveUserInsuracne(ctx, data) {
            axios.post('api/insurance/create', data)
        }
    },
    getters: {
        userInsurance(state) {
            return state.userInsuranceData
        },

    }
}
