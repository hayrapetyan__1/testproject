@extends('layouts.app')
@section('title', 'Login')
@section('content')
    <login
        route_login="{{route('login')}}"
        old_email="{{old('email')}}"
        old_remember="{{old('remember')}}"
        route_has="{{Route::has('password.request')}}"
        password_request="{{route('password.request')}}"
        email_error="{{$errors->has('email')}}"
        password_error="{{$errors->has('password')}}"
        message="{{json_encode($errors->all())}}">
    </login>
@endsection
