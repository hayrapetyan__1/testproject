@extends('layouts.app')
@section('title', 'Register Car')
@section('content')
<div class="container">
    <car-register auth_user="{{auth()->user()}}"></car-register>
</div>
@endsection
