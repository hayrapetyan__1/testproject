<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
});

Route::get('/marks', 'MarkController@get');
Route::get('/types', 'TypeController@get');
Route::get('/drivetrains', 'DrivetrainController@get');
Route::post('/upload', 'InsuranceController@fileUpload');

Route::group(['prefix' => 'insurance'], function () {
    Route::post('/create', 'InsuranceController@create');
    Route::get('/get', 'InsuranceController@get');
});

