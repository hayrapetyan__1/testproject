<?php

use App\Models\Type;
use Illuminate\Database\Seeder;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $models = [
            0 => ['name' => '3 Series', 'mark_id' => 1],
            1 => ['name' => '5 Series', 'mark_id' => 1],
            2 => ['name' => '7 Series', 'mark_id' => 1],
            3 => ['name' => 'C Class', 'mark_id' => 2],
            4 => ['name' => 'E Class', 'mark_id' => 2],
            5 => ['name' => 'S Class', 'mark_id' => 2],
            6 => ['name' => 'Wrangler', 'mark_id' => 3],
            7 => ['name' => 'Cherokee', 'mark_id' => 3],
            8 => ['name' => 'Grand Cherokee', 'mark_id' => 3],
        ];
        foreach ($models as $model) {
            Type::firstOrCreate($model);
        }
    }
}
