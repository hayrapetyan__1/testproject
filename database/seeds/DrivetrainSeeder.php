<?php

use App\Models\Drivetrain;
use Illuminate\Database\Seeder;

class DrivetrainSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $drivetrains = [
            0 => ['name' => '2x4', 'type_id' => 9],
            1 => ['name' => '4x4', 'type_id' => 9],
        ];
        foreach ($drivetrains as $drivetrain) {
            Drivetrain::firstOrCreate($drivetrain);
        }
    }
}
