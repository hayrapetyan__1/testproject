<?php

use App\Models\Mark;
use Illuminate\Database\Seeder;

class MarkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $makes = [
            0 => ['name' => 'BMW'],
            1 => ['name' => 'Mercedes'],
            2 => ['name' => 'Jeep'],
        ];
        foreach ($makes as $make) {
            Mark::firstOrCreate($make);
        }
    }
}
