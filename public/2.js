(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[2],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/carRegister.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/carRegister.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "carRegister",
  props: {
    auth_user: {
      required: true
    }
  },
  data: function data() {
    return {
      carData: {
        mark: '',
        type: '',
        milage: '',
        buy_date: '',
        color: '',
        drivetrain: '',
        user_id: ''
      },
      step: 1,
      showAlert: false
    };
  },
  watch: {
    'carData.buy_date': function carDataBuy_date(val) {
      var _this = this;

      this.carData.buy_date = val ? this.$moment(val).format('YYYY-MM-DD') : null;
    },
    'carData.milage': function carDataMilage(val) {
      if (val > 100000) {
        this.showAlert = true;
      }
    }
  },
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])(['carMarks', 'carTypes', 'carDrivetrains', 'userInsurance'])), {}, {
    dropOptions: function dropOptions() {
      return {
        url: '/api/upload',
        createImageThumbnails: true,
        dictDefaultMessage: "Attach Images",
        acceptedFiles: ".jpg,.png,.jpeg,.gif",
        uploadMultiple: false,
        addRemoveLinks: true,
        clickable: true
      };
    }
  }),
  methods: {
    saveInsurance: function saveInsurance() {
      this.$store.dispatch('saveUserInsuracne', this.carData);
    },
    getDrivetrains: function getDrivetrains(type) {
      this.carData.drivetrain = '';
      this.$store.dispatch('getDrivetrain', type);
      this.$store.dispatch('saveUserInsuracne', this.carData);
    },
    nextStep: function nextStep() {
      var _this2 = this;

      this.$validator.validateAll('form').then(function (result) {
        if (_this2.step == 1 && result) {
          _this2.step = 2;
        } else if (_this2.step == 2 && result) {
          _this2.step = 'finish';
        }
      });
    },
    getTypes: function getTypes(mark) {
      this.carData.type = '';
      this.$store.dispatch('getTypes', mark);
      this.$store.dispatch('saveUserInsuracne', this.carData);
    }
  },
  created: function created() {
    var _this3 = this;

    this.$store.dispatch('getMarks');
    this.carData.user_id = this.auth_user ? JSON.parse(this.auth_user).id : null;
    this.$store.dispatch('getUserInsurance', this.carData.user_id).then(function (response) {
      if (response.data) {
        _this3.carData = _this3.userInsurance;

        _this3.$store.dispatch('getTypes', _this3.carData.mark);

        response.data.type == 9 ? _this3.$store.dispatch('getDrivetrain', response.data.type) : false;
      }
    });
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/carRegister.vue?vue&type=template&id=55461e1c&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/carRegister.vue?vue&type=template&id=55461e1c& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _vm.showAlert
      ? _c(
          "div",
          {
            staticClass: "alert alert-info alert-dismissible fade show",
            attrs: { role: "alert" }
          },
          [_vm._v("\n        We can insure your car\n        "), _vm._m(0)]
        )
      : _vm._e(),
    _vm._v(" "),
    _c("div", { staticClass: "row justify-content-center" }, [
      _c("div", { staticClass: "col-12 col-md-8" }, [
        _c("div", { staticClass: "bg-white material-shadow rounded-lg" }, [
          _c("div", { staticClass: "card-header" }, [
            _c("h5", { staticClass: "m-0" }, [
              _vm._v("Make insurance step " + _vm._s(_vm.step))
            ])
          ]),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "card-body",
              class: _vm.step == "finish" ? "p-0" : false
            },
            [
              _c(
                "form",
                { attrs: { "data-vv-scope": "form" } },
                [
                  _vm.step == 1
                    ? [
                        _c("div", { staticClass: "row" }, [
                          _c("div", { staticClass: "col" }, [
                            _c("div", { staticClass: "form-group" }, [
                              _c("label", { attrs: { for: "mark" } }, [
                                _vm._v("mark")
                              ]),
                              _vm._v(" "),
                              _c(
                                "select",
                                {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.carData.mark,
                                      expression: "carData.mark"
                                    },
                                    {
                                      name: "validate",
                                      rawName: "v-validate",
                                      value: "required",
                                      expression: "'required'"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: { name: "mark", id: "mark" },
                                  on: {
                                    change: [
                                      function($event) {
                                        var $$selectedVal = Array.prototype.filter
                                          .call($event.target.options, function(
                                            o
                                          ) {
                                            return o.selected
                                          })
                                          .map(function(o) {
                                            var val =
                                              "_value" in o ? o._value : o.value
                                            return val
                                          })
                                        _vm.$set(
                                          _vm.carData,
                                          "mark",
                                          $event.target.multiple
                                            ? $$selectedVal
                                            : $$selectedVal[0]
                                        )
                                      },
                                      function($event) {
                                        return _vm.getTypes(_vm.carData.mark)
                                      }
                                    ]
                                  }
                                },
                                [
                                  _c(
                                    "option",
                                    {
                                      attrs: {
                                        value: "",
                                        disabled: "",
                                        selected: ""
                                      }
                                    },
                                    [_vm._v("Choose car mark")]
                                  ),
                                  _vm._v(" "),
                                  _vm._l(_vm.carMarks, function(mark) {
                                    return _c(
                                      "option",
                                      { domProps: { value: mark.id } },
                                      [_vm._v(_vm._s(mark.name))]
                                    )
                                  })
                                ],
                                2
                              ),
                              _vm._v(" "),
                              _vm.errors.items.find(function(item) {
                                return item.field == "mark"
                              })
                                ? _c("span", { staticClass: "text-danger" }, [
                                    _vm._v(
                                      "\n                                            " +
                                        _vm._s(
                                          this.errors.items.find(function(
                                            item
                                          ) {
                                            return item.field == "mark"
                                          }).msg
                                        ) +
                                        "\n                                        "
                                    )
                                  ])
                                : _vm._e()
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "col" }, [
                            _c("div", { staticClass: "form-group" }, [
                              _c("label", { attrs: { for: "type" } }, [
                                _vm._v("Model")
                              ]),
                              _vm._v(" "),
                              _c(
                                "select",
                                {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.carData.type,
                                      expression: "carData.type"
                                    },
                                    {
                                      name: "validate",
                                      rawName: "v-validate",
                                      value: "required",
                                      expression: "'required'"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: { id: "type", name: "type" },
                                  on: {
                                    change: [
                                      function($event) {
                                        var $$selectedVal = Array.prototype.filter
                                          .call($event.target.options, function(
                                            o
                                          ) {
                                            return o.selected
                                          })
                                          .map(function(o) {
                                            var val =
                                              "_value" in o ? o._value : o.value
                                            return val
                                          })
                                        _vm.$set(
                                          _vm.carData,
                                          "type",
                                          $event.target.multiple
                                            ? $$selectedVal
                                            : $$selectedVal[0]
                                        )
                                      },
                                      function($event) {
                                        return _vm.getDrivetrains(
                                          _vm.carData.type
                                        )
                                      }
                                    ]
                                  }
                                },
                                [
                                  _c(
                                    "option",
                                    {
                                      attrs: {
                                        value: "",
                                        disabled: "",
                                        selected: ""
                                      }
                                    },
                                    [_vm._v("Choose car model")]
                                  ),
                                  _vm._v(" "),
                                  _vm._l(_vm.carTypes, function(type) {
                                    return _c(
                                      "option",
                                      { domProps: { value: type.id } },
                                      [_vm._v(_vm._s(type.name))]
                                    )
                                  })
                                ],
                                2
                              ),
                              _vm._v(" "),
                              _vm.errors.items.find(function(item) {
                                return item.field == "type"
                              })
                                ? _c("span", { staticClass: "text-danger" }, [
                                    _vm._v(
                                      "\n                                            " +
                                        _vm._s(
                                          _vm.errors.items.find(function(item) {
                                            return item.field == "type"
                                          }).msg
                                        ) +
                                        "\n                                        "
                                    )
                                  ])
                                : _vm._e()
                            ])
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "row" }, [
                          _c("div", { staticClass: "col" }, [
                            _c("div", { staticClass: "form-group" }, [
                              _c("label", { attrs: { for: "milage" } }, [
                                _vm._v("Milage")
                              ]),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.carData.milage,
                                    expression: "carData.milage"
                                  },
                                  {
                                    name: "validate",
                                    rawName: "v-validate",
                                    value: "required",
                                    expression: "'required'"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "number",
                                  id: "milage",
                                  placeholder: "Choose car milage",
                                  name: "milage"
                                },
                                domProps: { value: _vm.carData.milage },
                                on: {
                                  change: function($event) {
                                    return _vm.saveInsurance()
                                  },
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.carData,
                                      "milage",
                                      $event.target.value
                                    )
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _vm.errors.items.find(function(item) {
                                return item.field == "milage"
                              })
                                ? _c("span", { staticClass: "text-danger" }, [
                                    _vm._v(
                                      "\n                                            " +
                                        _vm._s(
                                          _vm.errors.items.find(function(item) {
                                            return item.field == "milage"
                                          }).msg
                                        ) +
                                        "\n                                        "
                                    )
                                  ])
                                : _vm._e()
                            ])
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "col" }, [
                            _c(
                              "div",
                              { staticClass: "form-group" },
                              [
                                _c("label", { attrs: { for: "picker" } }, [
                                  _vm._v("Buying Date")
                                ]),
                                _vm._v(" "),
                                _c("datepicker", {
                                  directives: [
                                    {
                                      name: "validate",
                                      rawName: "v-validate",
                                      value: "required",
                                      expression: "'required'"
                                    }
                                  ],
                                  attrs: {
                                    "input-class": "form-control",
                                    id: "picker",
                                    placeholder: "Choose buying date",
                                    name: "date"
                                  },
                                  on: {
                                    selected: function($event) {
                                      return _vm.saveInsurance()
                                    }
                                  },
                                  model: {
                                    value: _vm.carData.buy_date,
                                    callback: function($$v) {
                                      _vm.$set(_vm.carData, "buy_date", $$v)
                                    },
                                    expression: "carData.buy_date"
                                  }
                                }),
                                _vm._v(" "),
                                _vm.errors.items.find(function(item) {
                                  return item.field == "date"
                                })
                                  ? _c("span", { staticClass: "text-danger" }, [
                                      _vm._v(
                                        "\n                                            " +
                                          _vm._s(
                                            _vm.errors.items.find(function(
                                              item
                                            ) {
                                              return item.field == "date"
                                            }).msg
                                          ) +
                                          "\n                                        "
                                      )
                                    ])
                                  : _vm._e()
                              ],
                              1
                            )
                          ])
                        ])
                      ]
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.step == 2
                    ? [
                        _c("div", { staticClass: "row" }, [
                          _c("div", { staticClass: "col" }, [
                            _c("div", { staticClass: "form-group" }, [
                              _c("label", { attrs: { for: "color" } }, [
                                _vm._v("Color")
                              ]),
                              _vm._v(" "),
                              _c(
                                "select",
                                {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.carData.color,
                                      expression: "carData.color"
                                    },
                                    {
                                      name: "validate",
                                      rawName: "v-validate",
                                      value: "required",
                                      expression: "'required'"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  attrs: { name: "color", id: "color" },
                                  on: {
                                    change: [
                                      function($event) {
                                        var $$selectedVal = Array.prototype.filter
                                          .call($event.target.options, function(
                                            o
                                          ) {
                                            return o.selected
                                          })
                                          .map(function(o) {
                                            var val =
                                              "_value" in o ? o._value : o.value
                                            return val
                                          })
                                        _vm.$set(
                                          _vm.carData,
                                          "color",
                                          $event.target.multiple
                                            ? $$selectedVal
                                            : $$selectedVal[0]
                                        )
                                      },
                                      function($event) {
                                        return _vm.saveInsurance()
                                      }
                                    ]
                                  }
                                },
                                [
                                  _c(
                                    "option",
                                    {
                                      attrs: {
                                        value: "",
                                        disabled: "",
                                        selected: ""
                                      }
                                    },
                                    [_vm._v("Choose car color")]
                                  ),
                                  _vm._v(" "),
                                  _c("option", { attrs: { value: "white" } }, [
                                    _vm._v("White")
                                  ]),
                                  _vm._v(" "),
                                  _c("option", { attrs: { value: "silver" } }, [
                                    _vm._v("Silver")
                                  ]),
                                  _vm._v(" "),
                                  _c("option", { attrs: { value: "black" } }, [
                                    _vm._v("Black")
                                  ]),
                                  _vm._v(" "),
                                  _c("option", { attrs: { value: "other" } }, [
                                    _vm._v("Other")
                                  ])
                                ]
                              ),
                              _vm._v(" "),
                              _vm.errors.items.find(function(item) {
                                return item.field == "color"
                              })
                                ? _c("span", { staticClass: "text-danger" }, [
                                    _vm._v(
                                      "\n                                            " +
                                        _vm._s(
                                          _vm.errors.items.find(function(item) {
                                            return item.field == "color"
                                          }).msg
                                        ) +
                                        "\n                                        "
                                    )
                                  ])
                                : _vm._e()
                            ])
                          ]),
                          _vm._v(" "),
                          Object.keys(_vm.carDrivetrains).length
                            ? _c("div", { staticClass: "col" }, [
                                _c("div", { staticClass: "form-group" }, [
                                  _c(
                                    "label",
                                    { attrs: { for: "drivetrain" } },
                                    [_vm._v("Drivetrain")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "select",
                                    {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.carData.drivetrain,
                                          expression: "carData.drivetrain"
                                        },
                                        {
                                          name: "validate",
                                          rawName: "v-validate",
                                          value: "required",
                                          expression: "'required'"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      attrs: {
                                        id: "drivetrain",
                                        name: "drivetrain"
                                      },
                                      on: {
                                        change: [
                                          function($event) {
                                            var $$selectedVal = Array.prototype.filter
                                              .call(
                                                $event.target.options,
                                                function(o) {
                                                  return o.selected
                                                }
                                              )
                                              .map(function(o) {
                                                var val =
                                                  "_value" in o
                                                    ? o._value
                                                    : o.value
                                                return val
                                              })
                                            _vm.$set(
                                              _vm.carData,
                                              "drivetrain",
                                              $event.target.multiple
                                                ? $$selectedVal
                                                : $$selectedVal[0]
                                            )
                                          },
                                          function($event) {
                                            return _vm.saveInsurance()
                                          }
                                        ]
                                      }
                                    },
                                    [
                                      _c(
                                        "option",
                                        {
                                          attrs: {
                                            value: "",
                                            disabled: "",
                                            selected: ""
                                          }
                                        },
                                        [_vm._v("Choose car drivetrain")]
                                      ),
                                      _vm._v(" "),
                                      _vm._l(_vm.carDrivetrains, function(
                                        drivetrain
                                      ) {
                                        return _c(
                                          "option",
                                          {
                                            domProps: { value: drivetrain.id }
                                          },
                                          [_vm._v(_vm._s(drivetrain.name))]
                                        )
                                      })
                                    ],
                                    2
                                  ),
                                  _vm._v(" "),
                                  _vm.errors.items.find(function(item) {
                                    return item.field == "drivetrain"
                                  })
                                    ? _c(
                                        "span",
                                        { staticClass: "text-danger" },
                                        [
                                          _vm._v(
                                            "\n                                            " +
                                              _vm._s(
                                                _vm.errors.items.find(function(
                                                  item
                                                ) {
                                                  return (
                                                    item.field == "drivetrain"
                                                  )
                                                }).msg
                                              ) +
                                              "\n                                        "
                                          )
                                        ]
                                      )
                                    : _vm._e()
                                ])
                              ])
                            : _vm._e()
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "row" }, [
                          _c("div", { staticClass: "col" }, [
                            _c(
                              "div",
                              { staticClass: "form-group" },
                              [
                                _c("label", { attrs: { for: "upload" } }, [
                                  _vm._v("Upload image")
                                ]),
                                _vm._v(" "),
                                _c("vueDropzone", {
                                  attrs: {
                                    id: "upload",
                                    options: _vm.dropOptions
                                  }
                                })
                              ],
                              1
                            )
                          ])
                        ])
                      ]
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.step == "finish"
                    ? _c("img", {
                        staticClass: "img-fluid",
                        attrs: {
                          src:
                            "https://steamuserimages-a.akamaihd.net/ugc/942834527348723621/619B7C8C2CB26B2E0403C2F8AD83A973571E1B5F/"
                        }
                      })
                    : _c("div", { staticClass: "row mt-4" }, [
                        _c("div", { staticClass: "col text-right" }, [
                          _vm.step == 2
                            ? _c(
                                "button",
                                {
                                  staticClass: "btn btn-primary",
                                  on: {
                                    click: function($event) {
                                      $event.preventDefault()
                                      return _vm.nextStep()
                                    }
                                  }
                                },
                                [_vm._v("Get quote")]
                              )
                            : _c(
                                "button",
                                {
                                  staticClass: "btn btn-primary",
                                  on: {
                                    click: function($event) {
                                      $event.preventDefault()
                                      return _vm.nextStep()
                                    }
                                  }
                                },
                                [_vm._v("Next")]
                              )
                        ])
                      ])
                ],
                2
              )
            ]
          )
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "close",
        attrs: {
          type: "button",
          "data-dismiss": "alert",
          "aria-label": "Close"
        }
      },
      [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("×")])]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        )
      }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./resources/js/components/carRegister.vue":
/*!*************************************************!*\
  !*** ./resources/js/components/carRegister.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _carRegister_vue_vue_type_template_id_55461e1c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./carRegister.vue?vue&type=template&id=55461e1c& */ "./resources/js/components/carRegister.vue?vue&type=template&id=55461e1c&");
/* harmony import */ var _carRegister_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./carRegister.vue?vue&type=script&lang=js& */ "./resources/js/components/carRegister.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _carRegister_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _carRegister_vue_vue_type_template_id_55461e1c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _carRegister_vue_vue_type_template_id_55461e1c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/carRegister.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/carRegister.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/carRegister.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_carRegister_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./carRegister.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/carRegister.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_carRegister_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/carRegister.vue?vue&type=template&id=55461e1c&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/carRegister.vue?vue&type=template&id=55461e1c& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_carRegister_vue_vue_type_template_id_55461e1c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./carRegister.vue?vue&type=template&id=55461e1c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/carRegister.vue?vue&type=template&id=55461e1c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_carRegister_vue_vue_type_template_id_55461e1c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_carRegister_vue_vue_type_template_id_55461e1c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);